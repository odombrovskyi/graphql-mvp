<?php

namespace App\Mutations;

use Overblog\GraphQLBundle\Definition\Argument;

class PriceMutation
{

    public function calculatePriceWithDiscount(Argument $args)
    {
        if ($args['input']['price'] > 22) {
            return [
                'error' => 'test'
            ];
        }
        return [
            'total_price' => 2,
            'discount' => 20
        ];
    }
}